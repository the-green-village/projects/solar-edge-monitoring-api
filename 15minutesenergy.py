import requests
import pandas as pd
import datetime as dt

from tgvfunctions import tgvfunctions
import projectsecrets

# Create tgvfunctions object
tgv = tgvfunctions.TGVFunctions(projectsecrets.TOPIC)

# The data of a measurement (say a measurement at 10:15:00) becomes available about 10 minutes after (so at 10:25:00)
# The Site Energy API only supports a start and end data (so no specific time on that date). We filter for the last data point later.
endDate = dt.datetime.now().date() - dt.timedelta(days=0)
startDate = endDate - dt.timedelta(days=1)

# Do the request

requesturl = "https://monitoringapi.solaredge.com/site/"+projectsecrets.INSTALLATION_ID+"/energy.json?timeUnit=QUARTER_OF_AN_HOUR&endDate=" + str(endDate) + "&startDate="+str(startDate) + "&api_key="+projectsecrets.PROJECT_API_KEY
res = requests.get(requesturl).json()
data = pd.DataFrame(res["energy"]["values"])

# Filter out last measured datapoint
time = tgv.round_time(roundTo=900, utc=True)
datapoint = data[data["date"] == str(time)]
datapoint = tgv.iso_to_epoch_ms(datapoint, "date")

producer = tgv.make_producer('pal_solaredge-producer')

# check for none types, and replace with 0
datapoint = datapoint.fillna(float(0))

value = {
            "project_id": "pretaloger",
            "application_id": "solar edge",
            "device_id": "58604",
            "timestamp": datapoint.iloc[0]["epochms"],
            "measurements": [
                {
                    "measurement_id": "energy",
                    "measurement_description": "energy measurement",
                    "value": datapoint.iloc[0]["value"],
                    "unit": "Wh"
                }
            ]
}
tgv.produce(producer, value)
producer.flush()
