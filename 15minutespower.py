import requests
import pandas as pd
import datetime as dt

from tgvfunctions import tgvfunctions
import projectsecrets

# Create tgvfunctions object
tgv = tgvfunctions.TGVFunctions(projectsecrets.TOPIC)

# The data of a measurement (say a measurement at 10:15:00) becomes available about 10 minutes after (so at 10:25:00)
# Get timeframe, I put in a time difference of 5 minutes to make sure the last datapoint is included (instead of up to the datapoint)
Lastdatapoint = tgv.round_time(roundTo=900, utc=True)
lastdatapoint = Lastdatapoint + dt.timedelta(minutes=5)
penultimatedatapoint = lastdatapoint - dt.timedelta(minutes=15)

requesturl = "https://monitoringapi.solaredge.com/site/"+projectsecrets.INSTALLATION_ID+"/power.json?timeUnit=QUARTER_OF_AN_HOUR&startTime=" + str(penultimatedatapoint.date()) + "%20" + str(penultimatedatapoint.time()) + "&endTime="+str(lastdatapoint.date())+"%20" + str(lastdatapoint.time()) + "&api_key="+projectsecrets.PROJECT_API_KEY
res = requests.get(requesturl).json()
data = pd.DataFrame(res["power"]["values"])
datapoint = data[data["date"] == str(Lastdatapoint)]
datapoint = tgv.iso_to_epoch_ms(datapoint, "date")

producer = tgv.make_producer('pal_solaredge-producer')

# check for none types, and replace with 0
datapoint = datapoint.fillna(float(0))

value = {
            "project_id": "pretaloger",
            "application_id": "solar edge",
            "device_id": "58604",
            "timestamp": datapoint.iloc[0]["epochms"],
            "measurements": [
                {
                    "measurement_id": "power",
                    "measurement_description": "power measurement",
                    "value": datapoint.iloc[0]["value"],
                    "unit": "W"
                }
            ]
}
tgv.produce(producer, value)
producer.flush()
