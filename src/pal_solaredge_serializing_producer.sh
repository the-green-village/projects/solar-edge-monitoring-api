#!/bin/bash
. ~/producers/.kafka_envs

cd ~/producers/solar-edge-monitoring-api/src
. ../env/bin/activate
python pal_solaredge_serializing_producer.py
