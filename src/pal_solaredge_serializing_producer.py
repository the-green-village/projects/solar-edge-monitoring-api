#!/usr/bin/python3
from json.decoder import JSONDecodeError
import requests
import json
import logging
import os
import time
from result import Result, Err, Ok, is_ok
from confluent_kafka import Producer
from typing import Union, Optional, Tuple, cast
from datetime import datetime, timedelta, date, timezone
from tgvfunctions import tgvfunctions
from projectsecrets import INSTALLATION_ID, PROJECT_API_KEY, TOPIC

# typedefs for generic numbers and TGVFunction objects
Number = Union[int, float]
TGVFunctions = tgvfunctions.TGVFunctions

FIFTEEN_MINUTES_MS = 15*60*1000

# Setting up the logger
logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO)
logger = logging.getLogger(__file__)


class SolarEdgeAPI:
    def __init__(self, meta = None):
        self.base_url = "https://monitoringapi.solaredge.com/site"
        self.install_id = INSTALLATION_ID
        self.api_key = PROJECT_API_KEY

        # Set the producer metadata
        self.project_id = "Inverters"
        self.application_id = "SolarEdge"
        self.device_id = "PV Inverter - SolarEdge"
        self.site_id =  "58604"
        self.device_type = "SE5000-ER-01"
        self.device_manufacturer = "SolarEdge"
        self.device_serial = "7F181DC8-7C"
        self.location_id = "Prêt-à-Loger"

        # Set the logfile
        self.logfile = f"{self.project_id}-solaredge-{self.site_id}.json"


    def construct_message(self, timestamp: Number, measurements: list) -> dict:
        value = {} # Initialize as empty dictionary
        value["project_id"] = self.project_id
        value["application_id"] = self.application_id
        value["application_description"] = f"Site ID {self.site_id}"
        value["device_id"] = self.device_id
        value["device_type"] = self.device_type
        value["device_manufacturer"] = self.device_manufacturer
        value["device_serial"] = self.device_serial
        value["location_id"] = self.location_id

        # Add the data and return
        value["timestamp"] = timestamp
        value["measurements"] = measurements
        return value

    def get_energy_data(self, start_date: date, end_date: date) -> Optional[dict]:
        query = f"energy.json?timeUnit=QUARTER_OF_AN_HOUR&endDate={str(end_date)}&startDate={str(start_date)}&api_key={self.api_key}"
        request_url = f"{self.base_url}/{self.install_id}/{query}"

        res = requests.get(request_url)
        if res.status_code != 200:
            logger.warning(f"Request for energy data failed with non-200 status code {res.status_code}")
            return None
        data = json.loads(res.text)
        return data


    def get_power_data(self, start_date: datetime, end_date: datetime) -> Optional[dict]:
        # Strip off trailing information on milliseconds and microseconds
        formatted_start_time = str(start_date.time()).split(".")[0]
        formatted_end_time = str(end_date.time()).split(".")[0]
        query = (f"power.json?timeUnit=QUARTER_OF_AN_HOUR"
                f"&startTime={str(start_date.date())}%20{formatted_start_time}"
                f"&endTime={str(end_date.date())}%20{formatted_end_time}"
                f"&api_key={self.api_key}")
        request_url = f"{self.base_url}/{self.install_id}/{query}"
        res = requests.get(request_url)
        if res.status_code != 200:
            logger.warning(f"Request for power data failed with non-200 status code {res.status_code}")
            return None
        data = json.loads(res.text)
        return data


    def get_site_overview(self, start_date, end_date):
        # Strip off trailing information on milliseconds and microseconds
        formatted_start_time = str(start_date.time()).split(".")[0]
        formatted_end_time = str(end_date.time()).split(".")[0]
        query = (f"overview.json?"
                f"&startTime={str(start_date.date())}%20{formatted_start_time}"
                f"&endTime={str(end_date.date())}%20{formatted_end_time}"
                f"&api_key={self.api_key}")
        request_url = f"{self.base_url}/{self.install_id}/{query}"
        res = requests.get(request_url)
        if res.status_code != 200:
            logger.warning(f"Request for power data failed with non-200 status code {res.status_code}")
            return None
        data = json.loads(res.text)
        return data


    def store_timestamp(self, timestamp: Number, key: str) -> None:
        cwd = os.path.dirname(os.path.realpath(__name__))
        if not os.path.isfile(f"{cwd}/{self.logfile}"):
            logger.warning(f"File {self.logfile} was not found and will be created.")
            cfg = {key: timestamp, "yield_last_updated": None}
        else:
            with open(self.logfile, 'r') as fb:
                cfg = json.load(fb)
                cfg[key] = timestamp
            
        with open(self.logfile, 'w') as fb:
            fb.write(json.dumps(cfg))
    

    def read_timestamp(self, key: str) -> Result[float, Exception]:
        # Return an Err if the file is not found
        cwd = os.path.dirname(os.path.realpath(__name__))
        if not os.path.isfile(f"{cwd}/{self.logfile}"):
            return Err(FileNotFoundError(f"{self.logfile} does not exist."))
        with open(self.logfile, 'r') as fb:
            try:
                cfg = json.load(fb)
            except:
                cfg = {}
        try:
            return Ok(float(cfg[key]))
        except KeyError as e:
            return Err(e)


    def reinit_timestamp_key(self, key: str) -> None:
        cwd = os.path.dirname(os.path.realpath(__name__))
        timestamp = time.time()*1000 - FIFTEEN_MINUTES_MS
        if not os.path.isfile(f"{cwd}/{self.logfile}"):
            logger.warning(f"File {self.logfile} was not found and will be created.")
            cfg = {key: timestamp}
        else:
            try:
                with open(self.logfile, 'r') as fb:
                    cfg = json.load(fb)
                    cfg[key] = timestamp # Add the key
            except (TypeError, JSONDecodeError):
                # If we encounter a type error, then the file was empty
                cfg = {key: timestamp}
        with open(self.logfile, 'w') as fb:
            fb.write(json.dumps(cfg))


def parse_data(data: dict) -> dict:
    """
    Parse the data from the API and return a dictionary containing 
    the datetimes as keys and the associated measured values as the values.
    """
    parsed_data_dict = {}
    for value in data["values"]:
        k, v = (value["date"], value["value"])
        parsed_data_dict[k] = v
    return parsed_data_dict


def parse_overview_data(data: dict) -> Tuple[datetime, Number]:
    """
    Parse the data from the API and return a dictionary containing 
    the datetimes as keys and the associated measured values as the values.
    """
    value = data["lifeTimeData"]["energy"]
    last_updated = datetime.fromisoformat(data["lastUpdateTime"])
    return last_updated, value


def round_minutes(base: int, date: datetime) -> datetime:
    """
    Take the datetime object and return it rounded to the nearest multiple of base minutes

    Parameters
    ----------
    `base: int`, the amount of minutes to round to
    `date: datetime`, the datetime object to round

    Returns
    -------
    `rounded_date: datetime`, the datetime object with seconds and microseconds set to 0 and the minutes set to 
        the nearest integer multiple of base.
    """
    mins = date.minute
    rounded_mins = (base*round(mins/base)) % 60 # Compute rounded time and map to 0..59 range
    rounded_date = date.replace(minute=rounded_mins, second=0, microsecond=0)
    return rounded_date


def produce_from_timestamp(timestamp_power: Number,
                           timestamp_energy: Number,
                           se_interface: SolarEdgeAPI,
                           producer: Producer,
                           tgv: TGVFunctions):
    """
    Read and produce all the data from a given timestamp up until now.

    Parameters
    ----------
    `timestamp: Number`, the timestamp in ms since epoch to start at with writing the data
    `se_interface: SolarEdgeAPI`, the interface object for dealing with the solaredge API
    `producer: Producer`, producer used to produce the messages to kafka
    `tgv: TGVFunctions`, handle for the tgv object for producing the messages to Kafka

    """
    # Get the data in 15 minute increments starting at the point specified through timestamp.
    start = datetime.fromtimestamp(timestamp_power/1000.0)
    until = (round_minutes(15, datetime.now()) + timedelta(minutes=5))
    logger.info(f"[Power]: Starting at {start} and looking up until {until}")
    
    all_power_data = se_interface.get_power_data(start, until)

    # Get the data in 15 minute increments starting at the point specified through timestamp.
    start = datetime.fromtimestamp(timestamp_energy/1000.0)
    until = (round_minutes(15, datetime.now()) + timedelta(minutes=5))
    logger.info(f"[Energy]: Starting at {start} and looking up until {until}")

    all_energy_data = se_interface.get_energy_data(start.date(), until.date())

    # Set this to None in the case that no data is produced
    last = None

    # Check if the API responded with something, then parse the data
    if all_energy_data is not None:
        data = parse_data(all_energy_data["energy"])
        for (k, v) in data.items():
            # Skipp all the points which do not have any data
            if v is None:
                continue
            measurement = tgvfunctions.generate_measurement_dict("Energy", v, "Wh")
            ts = datetime.fromisoformat(k).timestamp()*1000.0
            last = ts
            msg = se_interface.construct_message(ts, [measurement])
            tgv.produce_fast(producer, msg)

    if last is not None:
        se_interface.store_timestamp(last-FIFTEEN_MINUTES_MS, "last_produced_energy")

    # Set this to None in the case that no data is produced
    last = None

    # Check if the API responded with something, then parse the data
    if all_power_data is not None:
        data = parse_data(all_power_data["power"])
        for (k, v) in data.items():
            # Skip all the points which do not have any data
            if v is None:
                continue
            measurement = tgvfunctions.generate_measurement_dict("Power", v, "W")
            ts = datetime.fromisoformat(k).timestamp()*1000.0
            msg = se_interface.construct_message(ts, [measurement])
            last = ts # store the last written timestamp for starting next time
            tgv.produce_fast(producer, msg)

    if last is not None:
        se_interface.store_timestamp(last-FIFTEEN_MINUTES_MS, "last_produced_power")


def produce_historic(start_date: datetime,
                     se_interface: SolarEdgeAPI,
                     producer: Producer,
                     tgv: TGVFunctions):
    """
    start at `start_date` and produce backwards in time to produce the historic data
    """
    logger.info(f"Starting at {start_date}.")
    end_date = start_date - timedelta(days=31)

    while start_date > end_date:
        try:
            energy_data = se_interface.get_energy_data(end_date.date(), start_date.date())
            power_data = se_interface.get_power_data(end_date, start_date)

            # Check if the API responded with something, then parse the data
            if energy_data is not None:
                data = parse_data(energy_data["energy"])
                for (k, v) in data.items():
                    # Skipp all the points which do not have any data
                    if v is None:
                        continue
                    measurement = tgvfunctions.generate_measurement_dict("Energy", v, "Wh")
                    ts = datetime.fromisoformat(k).timestamp()*1000.0
                    msg = se_interface.construct_message(ts, [measurement])
                    print(json.dumps(msg, indent=2))
            else:
                logger.warning(f"Energy data for {end_date} - {start_date} could not be found")
        
            # Check if the API responded with something, then parse the data
            if power_data is not None:
                data = parse_data(power_data["power"])
                for (k, v) in data.items():
                    # Skip all the points which do not have any data
                    if v is None:
                        continue
                    measurement = tgvfunctions.generate_measurement_dict("Power", v, "W")
                    ts = datetime.fromisoformat(k).timestamp()*1000.0
                    msg = se_interface.construct_message(ts, [measurement])
                    print(json.dumps(msg, indent=2))
            else:
                logger.warning(f"Power data for {end_date} - {start_date} could not be found")

        except Exception as e:
            logger.warning(f"Caught exception {e} when handling {end_date} - {start_date}")

        start_date -= timedelta(days=1)


def produce_total_yield(start_date, end_date, se_interface: SolarEdgeAPI, producer: Producer, tgv: TGVFunctions):
    last_updated_previous = se_interface.read_timestamp("last_produced_yield")
    match last_updated_previous:
        case Ok(value):
            last_updated_previous = value
        case Err(value):
            logger.warning("Cannot find key " + str(value))
            se_interface.reinit_timestamp_key("last_produced_yield")
            last_updated_previous = se_interface.read_timestamp("last_produced_yield").unwrap()

    data = se_interface.get_site_overview(start_date, end_date)
    if data is not None: # If there is data
        last_updated, value = parse_overview_data(data["overview"])
        last_updated_epochms = last_updated.timestamp()*1000.0

        if last_updated_epochms > last_updated_previous:
            measurement = tgvfunctions.generate_measurement_dict("Total Yield", value, "Wh")
            msg = se_interface.construct_message(last_updated_epochms, [measurement])

            # Produce and Update the timestamp
            tgv.produce(producer, msg)
            se_interface.store_timestamp(last_updated_epochms-FIFTEEN_MINUTES_MS, "last_produced_yield")


def main():
    tgv = tgvfunctions.TGVFunctions(TOPIC)
    producer = tgv.make_producer("pal-solaredge-producer")
    se_interface = SolarEdgeAPI()

    # se_interface.store_timestamp(datetime(2024, 10, 25, 10, 0, 0, 0, timezone.utc).timestamp()*1000.0)
    ts_power = se_interface.read_timestamp("last_produced_power")
    match ts_power:
        case Ok(value):
            ts_power = value
        case Err(value):
            if isinstance(value, FileNotFoundError):
                logger.error(f"Exception {str(value)} occured, exiting with 1 exit code")
                exit(1)
            if isinstance(value, KeyError):
                se_interface.reinit_timestamp_key("last_produced_power")
            # The value is now safe to unwrap because we explicitly instantiated the key
            ts_power = se_interface.read_timestamp("last_produced_power").unwrap()

    ts_energy = se_interface.read_timestamp("last_produced_energy")
    match ts_energy:
        case Ok(value):
            ts_energy = value
        case Err(value):
            if isinstance(value, FileNotFoundError):
                logger.error(f"Exception {str(value)} occured, exiting with 1 exit code")
                exit(1)
            if isinstance(value, KeyError):
                se_interface.reinit_timestamp_key("last_produced_energy")
            # The value is now safe to unwrap because we explicitly instantiated the key
            ts_energy = se_interface.read_timestamp("last_produced_energy").unwrap()
    
    produce_from_timestamp(ts_power, ts_energy, se_interface, producer, tgv)
    producer.flush() # Ensure all messages are delivered

    start_date = round_minutes(5, datetime.now())
    end_date = start_date - timedelta(days=2)
    produce_total_yield(start_date, end_date, se_interface, producer, tgv)


if __name__ == "__main__":
    main()
