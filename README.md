# Pret A Loger - SolarEdge
This repository contains two python scripts that are run. These are the 15minutesenergy.py and the 15minutespower.py file. Both of these files require the variables PROJECT_API_KEY and INSTALLATION_ID to be set, next to the variables one would also need for other projects.

Since the data does not become available immediately on the SolarEdge platform, both scripts wait 10 minutes before requesting the data.

## 15minutesenergy.py
This produces the energy usage of the past 15 minutes to kafka every 15 minutes.

## 15minutespower.py
This produces the current power of the installation every 15 minutes.
